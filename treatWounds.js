// Treat wounds rolls
// --------------------------------------------
// Also works for creatures/NPCs
// Works with Natural Medicine (Nature) and
// Chirurgeon (Crafting)
// (c) Dalvyn
// --------------------------------------------


const usefulSkills = [
  {label: "Medicine", name: "Medicine", abr: "med", ability: "wis"},
  {label: "Crafting", name: "Crafting", abr: "cra", ability: "int"},
  {label: "Nature", name: "Nature", abr: "nat", ability: "wis"}
];

let targets = canvas.tokens.controlled;
if (targets.length != 1) {
  ui.notifications.warn("You must select exactly 1 token.");
  return;
}

let tokenSkills = usefulSkills.map(sk => getTokenSkill(token, sk));
let usableTokenSkills = tokenSkills.filter(sk => sk.rank > 0);
if (usableTokenSkills.length == 0) {
  ui.notifications.warn("This token has no usable skill for Treat Wounds!");
  return;
}

let treatWoundsOptions = [];
for (let {label, rank, mod} of usableTokenSkills) {
  treatWoundsOptions.push({
    desc: `${label} (${rankToText(rank)}, ${outputMod(mod)})`
  });
  for (let chosenRank = rank ; chosenRank >= 1 ; chosenRank--) {
    treatWoundsOptions.push({
      desc: `${label} DC ${DCByRank(chosenRank)} for 2d8${outputMod(bonusHPByRank(chosenRank))} HP`,
      label,
      chosenRank,
      rank,
      mod
    });
  }
}

let dialogContent =
 `<form>
    <p>Actor: ${token.name}</p>
    <p>Skill and target DC: <select id="skillSelect">`;
for (let i = 0 ; i < treatWoundsOptions.length ; i++) {
  let option = treatWoundsOptions[i];
  if (option.chosenRank === undefined) {
    dialogContent += `<optgroup label="${option.desc}"/>`;
  } else {
    dialogContent += `<option value="${i}">${option.desc}</option>`;
  }
}
dialogContent +=
     `</select></p>
    <p>Modifier: <input type="number" id="modRoll" style="width: 80px" value="0" /></p>
  </form>
      `;

let mustRoll = false;
new Dialog({
  title: "Treat Wounds",
  content: dialogContent,
  buttons: {
    yes: {
      icon: "<i class='fas fa-check'></i>",
      label: "Roll the dice!",
      callback: () => mustRoll = true
    },
    no: {
      icon: "<i class='fas fa-times'></i>",
      label: "Cancel",
    }
  },
  default: "yes",
  close: html => {
    if (mustRoll) {
      let chosenOption = treatWoundsOptions[html.find("#skillSelect")[0].value];
      let modRoll = html.find("#modRoll")[0].value;
      checkTreatWounds(token, chosenOption.label, chosenOption.rank, chosenOption.mod, modRoll, chosenOption.chosenRank);
    }
  }
}).render(true);

function checkTreatWounds (token, skillName, rank, mod, modRoll, chosenRank) {
  let dc = DCByRank(chosenRank);
  let output = `<h2>${token.name}: Treat Wounds</h2>`;
  output += `<h4 style="margin-bottom: 2px">${skillName} (${rankToText(rank)}, ${outputMod(mod)})`;
  if (modRoll != 0)
    output += `, mod ${outputMod(modRoll)}`;
  output += ` vs DC ${dc}</h4>`;

  let skillRoll = new Roll('1d20').roll().total;
  let skillResult = Number(skillRoll) + Number(mod) + Number(modRoll);
  let result = rollResult(skillResult, dc, skillRoll);

  let style=`border: 1px solid darkgrey;
             padding: 2px;
             margin: 0 0 4px 0;
             display: flex;
             justify-content: space-between;
             background-color: ${result.color}`;
  output += `<div style="${style}">
      <span>1d20 + ${mod}${modRoll == 0?"":" + " + modRoll} = <strong>${skillResult}</strong></span>
      <span>${result.resDesc}</span>
    </div>`;

  if (result.res === 1) {
    output += `<h4 style="margin-bottom: 2px">No effect.`;
  } else {
    let descTitre;
    let roll;
    if (result.res === 0) {
      descTitre = "target loses 1d8 HP.";
      roll = "1d8";
    } else {
      roll = result.res === 2 ? "2d8" : "4d8";
      let bonusHP = bonusHPByRank(chosenRank);
      if (bonusHP > 0)
        roll += " + " + bonusHP;
      descTitre = `target gains ${roll} HP.`;
    }
    output += `<h4 style="margin-bottom: 2px">Result: ${descTitre}</h4>`;
    let rollHP = new Roll(roll).roll().total;
    output += `<div style="${style}">
        <span>${roll} = <strong>${rollHP}</strong></span>
        <span>${result.res !== 0 ? "(×2 if extended for 1 hour)":""}</span>
      </div>`;
  }

  ChatMessage.create({
    user: game.user_id,
    content: output,
  }, {});
}

// Misc Functions

function rollResult (resRoll, dc, resd20) {
  let res;
  if (resRoll >= dc + 10)
    res = 3;
  else if (resRoll >= dc)
    res = 2;
  else if (resRoll > dc - 10)
    res = 1;
  else
    res = 0;
  if (resd20 == 20) {
    res++;
    if (res > 3) res = 3;
  } else if (resd20 == 1) {
    res--;
    if (res < 0) res = 0;
  }
  switch (res) {
    case 3: return {res, resDesc: "Critical success", color: "lime"};
    case 2: return {res, resDesc: "Success", color: "palegreen"};
    case 1: return {res, resDesc: "Failure", color: "khaki"};
    case 0: return {res, resDesc: "Critical failure", color: "darkkhaki"};
  }
}

function getTokenSkill (token, {label, name, abr, ability}) {
  let mod;
  let rank;
  if (token.actor.data.type === "npc") {
    // NPC
    let level = token.actor.data.data.details.level.value;
    let itemSkill = token.actor.data.items.find(item => item.name === name);
    if (itemSkill != undefined) {
      mod = itemSkill.data.mod.value;
      if (level >= 17)
        rank = 4;
      else if (level >= 9)
        rank = 3;
      else if (level >= 5)
        rank = 2;
      else
        rank = 1;
    } else {
      mod = token.actor.data.data.abilities[ability].mod;
      rank = 0;
    }
  } else {
    //PC
    mod = token.actor.data.data.skills[abr].totalModifier;
    rank = token.actor.data.data.skills[abr].rank;
  }
  return {label, rank, mod};
}

function outputMod (mod) {
  mod = Number(mod);
  if (isNaN(mod) || mod < 0)
    return "" + mod;
  return "+" + mod;
}

function bonusHPByRank (rank) {
  switch (rank) {
    case 4: return 50;
    case 3: return 30;
    case 2: return 10;
    case 1: return 0;
  }
}

function DCByRank (rank) {
  switch (rank) {
    case 4: return 40;
    case 3: return 30;
    case 2: return 20;
    case 1: return 15;
  }
}

function rankToText (rank) {
  switch (rank) {
    case 4: return "Legendary";
    case 3: return "Master";
    case 2: return "Expert";
    case 1: return "Trained";
    case 0: return "Untrained";
  }
}