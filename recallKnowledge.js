// Recall knowledge rolls for creatures
// --------------------------------------------
// Select the PC rolling the Recall knowledge
// Optionally, also select the target creature
// (c) Dalvyn, Zhamer Kaz
// --------------------------------------------

// Options for the dialog

let optsTraits = [
  {name: "Aberration (Occultism)", skills: ["Occultism"]},
  {name: "Animal (Nature)", skills: ["Nature"]},
  {name: "Astral (Occultism)", skills: ["Occultism"]},
  {name: "Beast (Arcana, Nature)", skills: ["Arcana", "Nature"]},
  {name: "Celestial (Religion)", skills: ["Religion"]},
  {name: "Construct (Arcana, Crafting)", skills: ["Arcana", "Crafting"]},
  {name: "Dragon (Arcana)", skills: ["Arcana"]},
  {name: "Elemental (Arcana, Nature)", skills: ["Arcana", "Nature"]},
  {name: "Ethereal (Occultism)", skills: ["Occultism"]},
  {name: "Fey (Nature)", skills: ["Nature"]},
  {name: "Fiend (Religion)", skills: ["Religion"]},
  {name: "Fungus (Nature)", skills: ["Nature"]},
  {name: "Humanoid (Society)", skills: ["Society"]},
  {name: "Monitor (Religion)", skills: ["Religion"]},
  {name: "Ooze (Occultism)", skills: ["Occultism"]},
  {name: "Plant (Nature)", skills: ["Nature"]},
  {name: "Spirit (Occultism)", skills: ["Occultism"]},
  {name: "Undead (Religion)", skills: ["Religion"]},
];

let optsLevels = [
  // Set DC based on creature level. Had to guess -1 and -2 as these are not in book. 
	{level: "-2", dc: "11"},
	{level: "-1", dc: "13"},
	{level: "0", dc: "14"},
	{level: "1", dc: "15"},
	{level: "2", dc: "16"},
	{level: "3", dc: "18"},
	{level: "4", dc: "19"},
	{level: "5", dc: "20"},
	{level: "6", dc: "22"},
	{level: "7", dc: "23"},
	{level: "8", dc: "24"},
	{level: "9", dc: "26"},
	{level: "10", dc: "27"},
	{level: "11", dc: "28"},
	{level: "12", dc: "30"},
	{level: "13", dc: "31"},
	{level: "14", dc: "32"},
	{level: "15", dc: "34"},
	{level: "16", dc: "35"},
	{level: "17", dc: "36"},
	{level: "18", dc: "38"},
	{level: "19", dc: "39"},
	{level: "20", dc: "40"},
	{level: "21", dc: "42"},
	{level: "22", dc: "44"},
	{level: "23", dc: "46"},
	{level: "24", dc: "48"},
	{level: "25", dc: "50"},
];

let optsRarities = [
  // creature rarity
  {rarity: "Common", dc: "0"},
  {rarity: "Uncommon", dc: "2"},
  {rarity: "Rare", dc: "5"},
  {rarity: "Unique", dc: "10"}
];

let optsQuestions = [
  // Type of question. 
	{question: "Yes or No (Is it immune to fire?)", dc: "0"},
	{question: "Specific (What are its resistances?)", dc: "2"},
	{question: "Generic (Tell me anything I can remember)", dc: "-2"},
];

// Main code

let targets = canvas.tokens.controlled;

let pc;
let creature;
for (let target of targets) {
  if (target.actor.data.type === "character" && pc === undefined) {
    pc = target.actor;
  } else if (target.actor.data.type === "npc" && creature === undefined) {
    creature = target.actor;
  }
}
if (pc === undefined) {
  ui.notifications.warn("You must target a player token.");
  return;
}

let mustRoll;
recallKnowledgeDialog(pc, creature).render(true);

// Dialog for Recall Knowledge

function recallKnowledgeDialog(pc, creature) {
  let content = `<p><strong>Character:</strong> ${pc.name}<br/>`;
  content += '<strong>Creature:</strong> ';
  content += creature === undefined ? 'unknown' : creature.name;
  content += '</p>';
  content += '<hr/>';

  content += '<div class="form-group">';
  content += '<p><label>Creature Trait : </label><select id="creatureskill">';
  for (let i = 0 ; i < optsTraits.length ; i++) {
    content += `<option value='${i}'`;
    if (creature != undefined &&
      creature.data.data.traits.traits.value.includes(
        firstWord(optsTraits[i].name).toLowerCase()
        )) {
      content += 'selected';
    }
    content += `>${optsTraits[i].name}</option>`;
  }
  content += '</select></p>';
  content += '<p><label>Creature Level : </label><select id="dclevel">';
  for (let i = 0 ; i < optsLevels.length ; i++) {
    content += `<option value='${i}'`;
    if (creature !== undefined &&
      creature.data.data.details.level.value == optsLevels[i].level) {
      content += ' selected';
    }
    content += `>${optsLevels[i].level}</option>`;
  }
  content += '</select></p>';
  content += '<p><label>Creature Rarity : </label><select id="creaturerarity">';
  for (let i = 0 ; i < optsRarities.length ; i++) {
    content += `<option value='${i}'`;
    if (creature !== undefined &&
      creature.data.data.traits.rarity.value == optsRarities[i].rarity.toLowerCase()) {
      content += ' selected';
    }
    content += `>${optsRarities[i].rarity}</option>`;
  }
  content += '</select></p>';
  content += '<p><label>Question Type : </label><select id="questiontype">';
  for (let i = 0 ; i < optsQuestions.length ; i++) {
    content += `<option value='${i}'>${optsQuestions[i].question}</option>`;
  }
  content += '</select></p>';
  content += '</div>';

  return new Dialog({
    title: "RECALL KNOWLEDGE",
    content: content,
    buttons: {
      roll: {
        icon: "<i class='fas fa-check'></i>",
        label: "Roll!",
        callback: () => mustRoll = true,
      },
      cancel: {
        icon: "<i class='fas fa-times'></i>",
        label: "Cancel",
        callback: () => mustRoll = false,
      },
    },
    default: "roll",
    close: html => {
      if (mustRoll) {
        let chosenTrait = optsTraits[html.find('#creatureskill')[0].value];
        let chosenLevel = optsLevels[html.find('#dclevel')[0].value];
        let chosenQuestion = optsQuestions[html.find('#questiontype')[0].value];
        let chosenRarity = optsRarities[html.find('#creaturerarity')[0].value];
        recallRoll(chosenTrait, chosenLevel, chosenQuestion, chosenRarity);
      }
    }
  });
}

// Roll Recall Knowledge

function recallRoll(chosenTrait, chosenLevel, chosenQuestion, chosenRarity) {
  let mod = -100;
  let rank;
  let skill;
  for (let sk of chosenTrait.skills) {
    let {mod:modSkill, rank:rankSkill} = getModRank(pc, sk);
    if (modSkill > mod) {
      mod = modSkill;
      rank = rankSkill;
      skill = sk;
    }
  }
  let totaldc = Number(chosenLevel.dc) + Number(chosenQuestion.dc) + Number(chosenRarity.dc);

  let output = '<h2>Recall knowledge</h2>';
  let description = `<strong>Character:</strong> ${pc.name}<br/>`;
  description += '<strong>Target:</strong> ';
  if (creature !== undefined) {
    description += creature.name + ', ';
  }
  description += `${firstWord(chosenTrait.name)} ${chosenLevel.level} `;
  description += `(${chosenRarity.rarity})<br/>`;
  description += `<strong>Skill:</strong> ${skill} (`;
  if (mod >= 0) {
    description += '+';
  }
  description += `${mod}, ${rankToText(rank)}) vs DC ${totaldc}`;

  output += testOutput (description, mod, totaldc);

  let chatData = {
    user: game.user._id,
    content: output,
    whisper: [game.user._id],
  };
  ChatMessage.create(chatData, {});
}

// Check output

function testOutput (description, mod, dc) {
  let roll = new Roll('1d20').roll();
  let resd20 = roll._result;
  let resRoll = Number(resd20) + mod;

  let style=`border: 1px solid darkgrey;
             padding: 2px;
             margin: 0 0 4px 0;
             display: flex;
             justify-content: space-between;`;
  let resDescription = "";
  
	let res = rollResult(resRoll, dc, resd20);
	resDescription = res.res;
	style += `background-color: ${res.color};`;
  

  let output = `<p style="margin-bottom: 2px">${description}</p>`;
  output += `<div style="${style}">
      <span>1d20 + ${mod} = <strong>${resRoll}</strong></span>
      <span>${resDescription}</span>
    </div>`
  return output;
}

// Misc functions

function firstWord (str) {
  return str.substr(0, str.indexOf(" "));
}

function getModRank (target, skill) {
  let abrev = skill.substring(0,3).toLowerCase();
  let mod = target.data.data.skills[abrev].value;
  let rank = target.data.data.skills[abrev].rank;
  return {mod,rank};
}

function rollResult (resRoll, dc, resd20) {
  let res;
  if (resRoll >= dc + 10)
    res = 3;
  else if (resRoll >= dc)
    res = 2;
  else if (resRoll > dc - 10)
    res = 1;
  else
    res = 0;
  if (resd20 == 20) {
    res++;
    if (res > 3) res = 3;
  } else if (resd20 == 1) {
    res--;
    if (res < 0) res = 0;
  }
  switch (res) {
    case 3: return {res: "Critical success", color: "lime"};
    case 2: return {res: "Success", color: "palegreen"};
    case 1: return {res: "Failure", color: "khaki"};
    case 0: return {res: "Critical failure", color: "darkkhaki"};
  }
}

function rankToText (rank) {
  switch (rank) {
    case 4: return "Legendary";
    case 3: return "Master";
    case 2: return "Expert";
    case 1: return "Trained";
    case 0: return "Untrained";
  }
}